<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Karyawan extends Model
{
    use Notifiable;

    protected $table = 'karyawan';
    protected $primaryKey = 'nip';
    protected $fillable = [
        'nip',
        'nama',
        'jabatan',
        'departemen',
        'tanggal_lahir',
        'alamat',
        'no_telp',
        'agama',
        'status',
        'foto_ktp',
    ];
}
