<?php

namespace App\Http\Controllers;

use App\Models\Karyawan;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class KaryawanController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Karyawan::get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('karyawan.index');
    }

    public function create()
    {
        return view('karyawan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nip' => 'required|unique:karyawan,nip|numeric',
            'nama' => 'required',
            'jabatan' => 'required',
            'departemen' => 'required',
            'tanggal_lahir' => 'required',
            'tahun_lahir' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required|numeric',
            'agama' => 'required',
            'status' => 'required',
            'foto_ktp' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ], [
            'nip.required' => 'NIP tidak boleh kosong',
            'nip.unique' => 'NIP sudah ada',
            'nip.numeric' => 'NIP harus berupa angka',
            'nama.required' => 'Nama tidak boleh kosong',
            'jabatan.required' => 'Jabatan tidak boleh kosong',
            'departemen.required' => 'Departemen tidak boleh kosong',
            'tanggal_lahir.required' => 'Tanggal lahir tidak boleh kosong',
            'tahun_lahir.required' => 'Tahun lahir tidak boleh kosong',
            'alamat.required' => 'Alamat tidak boleh kosong',
            'no_telp.required' => 'No. Telp tidak boleh kosong',
            'no_telp.numeric' => 'No. Telp harus berupa angka',
            'agama.required' => 'Agama tidak boleh kosong',
            'status.required' => 'Status tidak boleh kosong',
            'foto_ktp.required' => 'Foto KTP tidak boleh kosong',
            'foto_ktp.image' => 'Foto KTP harus berupa gambar',
            'foto_ktp.mimes' => 'Foto KTP harus berupa gambar dengan format jpeg, png, jpg',
            'foto_ktp.max' => 'Foto KTP maksimal berukuran 2 MB',
        ]);
        $tanggal_lahir = $request->tahun_lahir . '-' . $request->tanggal_lahir;

        $karyawan = new Karyawan();
        $karyawan->nip = $request->nip;
        $karyawan->nama = $request->nama;
        $karyawan->jabatan = $request->jabatan;
        $karyawan->departemen = $request->departemen;
        $karyawan->tanggal_lahir = $tanggal_lahir;
        $karyawan->alamat = $request->alamat;
        $karyawan->no_telp = $request->no_telp;
        $karyawan->agama = $request->agama;
        $karyawan->status = $request->status;

        if ($request->file('foto_ktp')) {
            $file = $request->file('foto_ktp');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/karyawan/', $filename);
            $karyawan->foto_ktp = $filename;
        }

        $karyawan->save();

        return redirect()->route('karyawan.index')->with('success-create', 'Data karyawan berhasil ditambahkan');
    }

    public function Edit($nip)
    {
        $karyawan = Karyawan::findorfail($nip);
        $tanggal_lahir = date('d-m', strtotime($karyawan->tanggal_lahir));
        $tahun_lahir = date('Y', strtotime($karyawan->tanggal_lahir));
        return view('karyawan.edit', compact('karyawan', 'tanggal_lahir', 'tahun_lahir'));
    }

    public function update(Request $request, $nip)
    {
        $request->validate([
            'nama' => 'required',
            'jabatan' => 'required',
            'departemen' => 'required',
            'tanggal_lahir' => 'required',
            'tahun_lahir' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required|numeric',
            'agama' => 'required',
            'status' => 'required',
            'foto_ktp' => 'image|mimes:jpeg,png,jpg|max:2048',
        ], [
            'nama.required' => 'Nama tidak boleh kosong',
            'jabatan.required' => 'Jabatan tidak boleh kosong',
            'departemen.required' => 'Departemen tidak boleh kosong',
            'tanggal_lahir.required' => 'Tanggal lahir tidak boleh kosong',
            'tahun_lahir.required' => 'Tahun lahir tidak boleh kosong',
            'alamat.required' => 'Alamat tidak boleh kosong',
            'no_telp.required' => 'No. Telp tidak boleh kosong',
            'no_telp.numeric' => 'No. Telp harus berupa angka',
            'agama.required' => 'Agama tidak boleh kosong',
            'status.required' => 'Status tidak boleh kosong',
            'foto_ktp.image' => 'Foto KTP harus berupa gambar',
            'foto_ktp.mimes' => 'Foto KTP harus berupa gambar dengan format jpeg, png, jpg',
            'foto_ktp.max' => 'Foto KTP maksimal berukuran 2 MB',
        ]);
        $tanggal_lahir = $request->tahun_lahir . '-' . $request->tanggal_lahir;

        $karyawan = Karyawan::find($nip);
        $karyawan->nama = $request->nama;
        $karyawan->jabatan = $request->jabatan;
        $karyawan->departemen = $request->departemen;
        $karyawan->tanggal_lahir = $tanggal_lahir;
        $karyawan->alamat = $request->alamat;
        $karyawan->no_telp = $request->no_telp;
        $karyawan->agama = $request->agama;
        $karyawan->status = $request->status;

        if ($request->file('foto_ktp')) {
            if ($request->oldfoto) {
                unlink('uploads/karyawan/' . $request->oldfoto);
            }
            $file = $request->file('foto_ktp');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/karyawan/', $filename);
            $karyawan->foto_ktp = $filename;
        }
        $karyawan->update();


        return redirect()->route('karyawan.index')->with('success-update', 'Data karyawan berhasil diubah');
    }

    public function destroy($nip)
    {
        $karyawan = Karyawan::find($nip);
        if ($karyawan->foto_ktp) {
            unlink('uploads/karyawan/' . $karyawan->foto_ktp);
        }
        $karyawan->delete();
        return redirect()->back();
    }

    public function detail_karyawan($nip)
    {
        $karyawan = Karyawan::find($nip);
        return response()->json($karyawan);
    }

    public function change_status($nip)
    {
        $karyawan = Karyawan::find($nip);
        if ($karyawan->status == 1) {
            $karyawan->status = 0;
        } else {
            $karyawan->status = 1;
        }
        $karyawan->update();
        return redirect()->back();
    }
}
