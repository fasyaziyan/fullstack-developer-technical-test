<?php

use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\KaryawanController;
use App\Http\Controllers\PositionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});
Route::prefix('karyawan')->group(function () {
    Route::get('/', [KaryawanController::class, 'index'])->name('karyawan.index');
    Route::get('/create', [KaryawanController::class, 'create'])->name('karyawan.create');
    Route::post('/', [KaryawanController::class, 'store'])->name('karyawan.store');
    Route::get('/edit/{nip}', [KaryawanController::class, 'edit'])->name('karyawan.edit');
    Route::post('/update/{nip}', [KaryawanController::class, 'update'])->name('karyawan.update');
    Route::post('/delete/{nip}', [KaryawanController::class, 'destroy'])->name('karyawan.destroy');
    Route::get('/detail/{nip}', [KaryawanController::class, 'detail_karyawan'])->name('karyawan.detail');
    Route::post('/change_status/{nip}', [KaryawanController::class, 'change_status'])->name('karyawan.change_status');
});

Route::resource('position', PositionController::class);
Route::resource('employee', EmployeeController::class);
