@extends('layout.app')
@section('content')
<style>
    .center {
        display: block;
        margin-left: auto;
        margin-right: auto;
        width: 50%;
    }

    .modal-content {
        font-weight: bold;
    }
</style>
<div class="grid-margin stretch-card">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Data Karyawan</h4>
        </div>
        <div class="card-body">
            <a class='btn btn-info btn-gradient-info' href="{{ route('karyawan.create') }}"><i
                    class='mdi mdi-plus menu-icon'></i>
                Tambah Karyawan</a>
            <br><br>
            <table class="display cell-border table" id="table">
                <thead>
                    <tr>
                        <th style="text-align: center;"> No </th>
                        <th style="width: 5%;"> NIP </th>
                        <th> Nama </th>
                        <th> Jabatan </th>
                        <th> Departemen </th>
                        <th> Status </th>
                        <th style="text-align: center; width:20%"> Action </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade bd-example-modal" id="karyawanModalDetail" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Informasi Karyawan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img src="" alt="avatar" class="rounded-circle center" style="width: 100px; height:100px"
                    id="karyawan-foto">
                <br>
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td style="width:30%">NIP</td>
                            <td id="karyawan-nip"></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td id="karyawan-nama"></td>
                        </tr>
                        <tr>
                            <td>Tanggal Lahir</td>
                            <td id="karyawan-tanggal-lahir"></td>
                        </tr>
                        <tr>
                            <td>Jabatan</td>
                            <td id="karyawan-jabatan"></td>
                        </tr>
                        <tr>
                            <td>Departement</td>
                            <td id="karyawan-departemen"></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td id="karyawan-alamat"></td>
                        </tr>
                        <tr>
                            <td>No. Telepon</td>
                            <td id="karyawan-no_telepon"></td>
                        </tr>
                        <tr>
                            <td>Agama</td>
                            <td id="karyawan-agama"></td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td id="karyawan-status"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('karyawan.index') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    className: 'text-center'
                },
                {
                    data: 'nip',
                    name: 'nip',
                    className: 'text-center'
                },
                {
                    data: 'nama',
                    name: 'nama',
                    className: 'text-center'
                },
                {
                    data: 'jabatan',
                    name: 'jabatan',
                    className: 'text-center'
                },
                {
                    data: 'departemen',
                    name: 'departemen',
                    className: 'text-center'
                },
                {
                    data: 'status',
                    render: function(data, type, row) {
                        if (row.status == 1) {
                            return '<a href="#" data-id="' + row.nip + '" class="badge badge-success change-status">Aktif</a>';
                        } else {
                            return '<a href="#" data-id="' + row.nip + '" class="badge badge-danger change-status">Tidak Aktif</a>';
                        }
                    },
                    className: 'text-center'
                }
            ],
            columnDefs: [{
                targets: 6,
                orderable: false,
                className: 'text-center',
                render: function(data, type, row) {
                    return `
                    <a href=":edit" class='btn btn-warning'><i class="far fa-edit" aria-hidden="true"></i></a>
                        <a href="#" class='btn btn-danger delete' data-id="${row.nip}"><i class="fas fa-trash" aria-hidden="true"></i></a>
                                <a href="javascript:void(0)" class='btn btn-info show-detail' data-id="${row.nip}"><i class="fas fa-info-circle" aria-hidden="true"></i></a>
                    `.replace(':edit', `{{ route('karyawan.edit', ':nip') }}`.replace(':nip', row.nip));
                }
            }]
        });
    });
</script>

<script>
    $(document).on('click', '.delete', function() {
        var nip = $(this).attr('data-id');
        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Setelah dihapus, Anda tidak akan dapat memulihkan data ini!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ya, hapus!",
            showClass: {
                popup: 'animate__animated animate__fadeInDown'
            }
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "{{ route('karyawan.destroy', ':nip') }}".replace(':nip', nip),
                    type: 'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "_method": "POST"
                    },
                    success: function(data) {
                        Swal.fire({
                            title: "Data Karyawan Berhasil Dihapus",
                            icon: "success",
                            showConfirmButton: false,
                            timer: 1500,
                        });
                        $('#table').DataTable().ajax.reload();
                    }
                });
            }
        });
    });
</script>
@if (Session::has('success-create'))
<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3500,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
    Toast.fire({
        icon: 'success',
        title: 'Data Karyawan Berhasil Ditambahkan'
    })
</script>
@elseif (Session::has('success-update'))
<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3500,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
    Toast.fire({
        icon: 'success',
        title: 'Data Karyawan Berhasil Diubah'
    })
</script>
@endif

<script>
    $(document).on('click', '.show-detail', function() {
        var nip = $(this).attr('data-id');
        $.ajax({
            url: "{{ route('karyawan.detail', ':nip') }}".replace(':nip', nip),
            type: 'GET',
            success: function(data) {
                $('#karyawanModalDetail').modal('show');
                $('#karyawan-nip').text(data.nip);
                $('#karyawan-nama').text(data.nama);
                $('#karyawan-jabatan').text(data.jabatan);
                $('#karyawan-departemen').text(data.departemen);
                $('#karyawan-tanggal-lahir').text(data.tanggal_lahir);
                $('#karyawan-alamat').text(data.alamat);
                $('#karyawan-no_telepon').text(data.no_telp);
                $('#karyawan-agama').text(data.agama);
                $('#karyawan-status').text({
                    '0': 'Karyawan Tidak Aktif',
                    '1': 'Karyawan Aktif'
                } [data.status]);
                $('#karyawan-foto').attr('src', "{{ asset('uploads/karyawan/') }}/" + data.foto_ktp);
            }
        });
    });
</script>

<script>
    $(document).on('click', '.change-status', function() {
        var nip = $(this).attr('data-id');
        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Status karyawan akan berubah!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ya, ubah!",
            showClass: {
                popup: 'animate__animated animate__fadeInDown'
            }
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "{{ route('karyawan.change_status', ':nip') }}".replace(':nip', nip),
                    type: 'POST',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "_method": "POST"
                    },
                    success: function(data) {
                        Swal.fire({
                            title: "Status Karyawan Berhasil Diubah",
                            icon: "success",
                            showConfirmButton: false,
                            timer: 1500,
                        });
                        $('#table').DataTable().ajax.reload();
                    }
                });
            }
        });
    });
</script>
@endsection
