@extends('layout.app')
@section('content')
<div class="grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <form action="{{ route('karyawan.update', $karyawan->nip) }}" method="post" enctype='multipart/form-data'>
                @csrf
                <div class="form-group">
                    <label>NIP</label>
                    <input class="form-control @error('nip') is-invalid @enderror" autocomplete="off" name="nip"
                        value="{{ $karyawan->nip }}" disabled
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
                </div>
                <div class="form-group">
                    <label>Nama Lengkap</label>
                    <input class="form-control @error('nama') is-invalid @enderror" autocomplete="off" name="nama"
                        value="{{ $karyawan->nama }}">
                    @error('nama')
                    <h6 class="text-danger">{{ $message }}</h6>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Jabatan</label>
                    <input class="form-control @error('jabatan') is-invalid @enderror" autocomplete="off" name="jabatan"
                        value="{{ $karyawan->jabatan }}">
                    @error('jabatan')
                    <h6 class="text-danger">{{ $message }}</h6>
                    @enderror
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Departement</label>
                            <input class="form-control @error('departemen') is-invalid @enderror" autocomplete="off"
                                name="departemen" value="{{ $karyawan->departemen }}">
                            @error('departemen')
                            <h6 class="text-danger">{{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input id="datepicker" class="form-control @error('tanggal_lahir') is-invalid @enderror"
                                autocomplete="off" name="tanggal_lahir" value="{{ $tanggal_lahir }}">
                            @error('tanggal_lahir')
                            <h6 class="text-danger">{{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Tahun Lahir</label>
                            <input id="datepicker-year" class="form-control @error('tahun_lahir') is-invalid @enderror"
                                autocomplete="off" name="tahun_lahir" value="{{ $tahun_lahir }}">
                            @error('tahun_lahir')
                            <h6 class="text-danger">{{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <textarea class="form-control @error('alamat') is-invalid @enderror" autocomplete="off"
                        name="alamat" value="{{ $karyawan->alamat }}">{{ $karyawan->alamat }}</textarea>
                    @error('alamat')
                    <h6 class="text-danger">{{ $message }}</h6>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Telepon</label>
                    <input class="form-control @error('no_telp') is-invalid @enderror" autocomplete="off" name="no_telp"
                        value="{{ $karyawan->no_telp }}"
                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');">
                    @error('no_telp')
                    <h6 class="text-danger">{{ $message }}</h6>
                    @enderror
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Agama</label>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="agama" value="Islam" {{
                                            $karyawan->agama=='Islam' ? 'checked' : '' }}>
                                        <label class="form-check-label">
                                            Islam
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="agama" value="Kristen" {{
                                            $karyawan->agama=='Kristen' ? 'checked' : '' }}>
                                        <label class="form-check-label">
                                            Kristen
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="agama" value="Katolik" {{
                                            $karyawan->agama=='Katolik' ? 'checked' : '' }}>
                                        <label class="form-check-label">
                                            Katolik
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="agama" value="Hindu" {{
                                            $karyawan->agama=='Hindu' ? 'checked' : '' }}>
                                        <label class="form-check-label">
                                            Hindu
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="agama" value="Budha" {{
                                            $karyawan->agama=='Budha' ? 'checked' : '' }}>
                                        <label class="form-check-label">
                                            Budha
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="agama" value="Konghucu" {{
                                            $karyawan->agama=='Konghucu' ? 'checked' : '' }}>
                                        <label class="form-check-label">
                                            Konghucu
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @error('agama')
                        <h6 class="text-danger">{{ $message }}</h6>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control" name="status">
                                <option disabled selected>Pilih Status Karyawan</option>
                                <option value="1" {{ $karyawan->status == "1" ? 'selected' : '' }}>Karyawan Aktif
                                </option>
                                <option value="0" {{ $karyawan->status == "0" ? 'selected' : '' }}>Karyawan Tidak Aktif
                                </option>
                            </select>
                            @error('status')
                            <h6 class="text-danger">{{ $message }}</h6>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Foto KTP</label>
                    <input type="hidden" name="oldfoto" value="{{ $karyawan->foto_ktp }}">
                    @if ($karyawan->foto_ktp)
                    <img src="{{ asset('uploads/karyawan/' . $karyawan->foto_ktp) }}" alt="{{ $karyawan->nama }}"
                        class="preview-image mb-3 col-sm-3">
                    @else
                    <img class="preview-image mb-3 col-sm-3">
                    @endif
                    <input class="form-control" autocomplete="off" name="foto_ktp" type="file"
                        value="{{ old('foto_ktp') }}" onchange="previewImage()">
                    @error('foto_ktp')
                    <h6 class="text-danger">{{ $message }}</h6>
                    @enderror
                </div>
                <div class="form-group"></div>
                <button type="submit" class="btn btn-primary mr-2 float-right">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    function previewImage() {
        const reader = new FileReader();
        reader.onload = function () {
            $('.preview-image').attr('src', reader.result);
        };
        reader.readAsDataURL(event.target.files[0]);
    }
</script>

<script>
    function alphaOnly(event) {
        var value = String.fromCharCode(event.which);
        var pattern = new RegExp(/[a-zåäö ]/i);
        return pattern.test(value);
    };
    $('.nama').keypress(function (e) {
        if (!alphaOnly(e)) {
            e.preventDefault();
        }
    });
</script>

<script>
    $(document).ready(function () {
        $('#datepicker').datepicker({
            format: "mm-dd",
            autoclose: true,
            // todayHighlight: true,
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('#datepicker-year').datepicker({
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years",
            endDate: '-17y',
        });
    });
</script>
@endsection

